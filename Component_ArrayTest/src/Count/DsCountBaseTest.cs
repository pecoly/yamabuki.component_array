﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Component.Array;

namespace Yamabuki.Component.ArrayTest
{
    [TestFixture]
    public class DsCountBaseTest
        : ArrayBaseTest
    {
        public DsCountBaseTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            count = CreateDsCount("c", rootSystem);
        }
        
        DsRootSystem rootSystem;
        DsCount count;
    }
}
