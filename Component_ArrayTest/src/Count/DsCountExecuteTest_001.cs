﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.ArrayTest
{
    [TestFixture]
    public class DsCountExecuteTest_001
        : ArrayBaseTest
    {
        public DsCountExecuteTest_001()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsIntegerArray
        ///  DsCount
        ///  DsTerminator
        ///  
        /// DsIntegerArray -> DsCount -> DsTerminator
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var i = this.CreateDsIntegerArrayMock("i", r);
            var c = this.CreateDsCount("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(i, 0, c, 0);
            DsComponentConnectionUtils.ConnectForData(c, 0, t, 0);

            //  プロパティの更新
            i.Value = new List<Int32>{ 1, 3, 5 };

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(t.Result, "3\r\n");
        }

    }
}
