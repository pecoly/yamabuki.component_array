﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Yamabuki.Component.Array;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Design.Utility;

namespace Yamabuki.Component.ArrayTest
{
    public class DsCountDoubleClickTest
        : ArrayBaseTest
    {
        public DsCountDoubleClickTest()
        {
            this.SetUp();
        }

        [SetUp]
        public void SetUp()
        {
            this.Initialize();
            rootSystem = CreateDsRootSystem("r");
            count = CreateTestingDsCount("c", rootSystem);
            count.DoubleClick();
        }

        [Test]
        public void DoubleClick()
        {
            Assert.IsTrue(count.ShowDialogCalled);
        }

        private TestingDsCount CreateTestingDsCount(String name, DsComponentImpl parent)
        {
            var com = new TestingDsCount();
            DsComponentCreationUtils.InitializeComponent(this.Collection, 0, 0, name, parent.Data, com);
            return com;
        }

        DsRootSystem rootSystem;
        TestingDsCount count;
    }

    public class TestingDsCount
        : DsCount
    {
        protected override void ShowDialog()
        {
            this.ShowDialogCalled = true;
        }

        public Boolean ShowDialogCalled;
    }
}
