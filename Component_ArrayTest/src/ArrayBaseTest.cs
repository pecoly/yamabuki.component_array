﻿using System;
using System.IO;
using System.Xml.Linq;

using Yamabuki.Component.Array;
using Yamabuki.Design;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Design.Container;
using Yamabuki.Design.Utility;
using Yamabuki.Task.Data;
using Yamabuki.Test;
using Yamabuki.Test.Component.Mock;

namespace Yamabuki.Component.ArrayTest
{
    public class ArrayBaseTest
        : BaseTest
    {
        String nameSpace = "Yamabuki.Component.ArrayTest";

        public override void Initialize()
        {
            base.Initialize();

            String project = "Component_ArrayTest";
            String name = "yamabuki.component_array";
            String target = "yamabuki";
            String bin = "bin\\Debug";
            Int32 pos;

            String path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            pos = path.IndexOf(project);
            if (pos != -1)
            {
                bin = path.Substring(pos + project.Length + 1);
            }

            pos = path.IndexOf(target, StringComparison.OrdinalIgnoreCase);
            if (pos == -1)
            {
                throw new DirectoryNotFoundException(path);
            }

            this.SolutionPath = path.Substring(0, pos + target.Length) + "\\" + name;
            this.ProjectPath = this.SolutionPath + "\\" + project;
            this.ExePath = Path.Combine(this.ProjectPath, bin);
        }

        public DsCount CreateDsCount(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsCount>(this.Collection, 20, 20, name, parent);
        }

        public override String ExePath { get; set; }
        public String SolutionPath { get; private set; }
        public String ProjectPath { get; private set; }
    }
}
