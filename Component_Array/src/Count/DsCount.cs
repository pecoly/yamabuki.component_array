﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Array
{
    public class DsCount
        : DsSimpleComponent_1_1
    {
        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal String Description
        {
            get
            {
                return "データ長を求めます。\r\n" + 
                    "例 : \r\n" + 
                    "入力 : [1 2 3]\r\n" + 
                    "出力 : 3";
            }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            this.ShowDialog();
            return null;
        }

        protected override void Initialize_1_1()
        {
        }

        protected virtual void ShowDialog()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsCount(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid);
        }
    }
}
