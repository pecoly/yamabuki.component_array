﻿using System;

using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Array
{
    internal class TsCount
        : TsTask_1_1
    {
        public TsCount(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
            : base(
            definitionPath,
            inputDataStoreGuid,
            outputDataStoreGuid)
        {
        }

        protected override void CheckInputDataList(
            TsDataList inputDataList)
        {
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            outputDataList = new TsDataListT<Int32>();
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList,
            TsDataList outputDataList)
        {
            var outputDataListT = outputDataList as TsDataListT<Int32>;
            outputDataListT.AddValue(inputDataList.Count);
        }
    }
}
