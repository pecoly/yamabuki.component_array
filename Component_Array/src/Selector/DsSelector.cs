﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Array
{
    public class DsSelector
        : DsSimpleComponent_2_1
    {
        public override int DefaultWidth
        {
            get { return 120; }
        }

        public override int DefaultHeight
        {
            get { return 60; }
        }

        internal String Description
        {
            get
            {
                return "配列の指定位置のデータを求めます。\r\n" +
                    "インデックスは0から始まる整数です。\r\n" +
                    "1番目を取得するときは0を、\r\n" +
                    "2番めを取得するときは1を指定してください。\r\n" +
                    "例 : \r\n" +
                    "入力1(データ) : [1 2 3 4 5 6]\r\n" +
                    "入力2(インデックス) : [0 5 1]\r\n" +
                    "出力 : [0 6 2]";
            }
        }

        protected override String InputName0
        {
            get { return "データ"; }
        }
        
        protected override String InputName1
        {
            get { return "インデックス"; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
                return null;
            }
        }

        protected override void Initialize_2_1()
        {
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsSelector(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsSelectorFunction);
        }
    }
}
