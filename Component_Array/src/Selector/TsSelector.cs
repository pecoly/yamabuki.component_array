﻿using System;

using Yamabuki.Component.Array.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Create;
using Yamabuki.Task.Function.Selector;

namespace Yamabuki.Component.Array
{
    internal class TsSelector
        : TsTask_2_1
    {
        public TsSelector(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsSelectorFunction func)
            : base(
            definitionPath,
            inputDataStoreGuid0,
            inputDataStoreGuid1,
            outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }

        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsSelectorFunction Func { get; private set; }

        protected override void CheckInputDataList(
            TsDataList inputDataList0,
            TsDataList inputDataList1)
        {
            if (inputDataList1.Type != typeof(Int32))
            {
                throw new TsInvalidDataTypeException(
                    this.DefinitionPath,
                    Resource.InvalidIndexDataType);
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore0 = this.GetInputDataStore(0);
            outputDataList = this.CreateFunc.Execute(inputDataStore0.DataList.Type);
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList0,
            TsDataList inputDataList1,
            TsDataList outputDataList)
        {
            try
            {
                this.Func.Execute(inputDataList0, inputDataList1, outputDataList);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new TsOutOfRangeException(
                    this.DefinitionPath,
                    "範囲外のインデックスが指定されました。"); 
            }
        }
    }
}
